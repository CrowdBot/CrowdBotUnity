# -*- coding: utf-8 -*-

# Learn more: https://github.com/kennethreitz/setup.py

from setuptools import setup, find_packages


with open('README.rst') as f:
    readme = f.read()

with open('LICENSE') as f:
    license = f.read()

setup(
    name='crowdbotsimcontrol',
    version='0.1.0',
    description='python API to control CrowdBotSim executable',
    long_description=readme,
    author='Fabien Grzeskowiak',
    author_email='fabien.grzeskowiak@inria.fr',
    url='https://gitlab.inria.fr',
    license=license,
    packages=find_packages(exclude=('tests', 'docs'))
)

